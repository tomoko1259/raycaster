#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <raylib.h>
#include <raymath.h>

#define SCREEN_WIDTH 800
#define SCREEN_HEIGHT 600
#define BUFFER_SIZE 10240
#define RAY_COUNT 120.0f
#define TURN_SPEED 100.0f
#define MOVE_SPEED 100.0f

typedef enum { NONE, WALL } Type;

typedef struct {
     Rectangle body;
     Type type;
} Block;

typedef struct {
     float x, y;
     float angle;
} Player;

Player player;
Block *map_block;

int dim;
int map_area;
int map_size;
int pixel_count;
char *map;
bool minimap = false;

void load_data() {
     FILE *data = fopen("data.conf", "r");
     if (!data) {
	  printf("Error opening data.conf for reading.\n");
	  exit(1);
     }
     
     char buffer[BUFFER_SIZE];
     
     fgets(buffer, BUFFER_SIZE, data);
     dim = atoi(buffer);
     
     fgets(buffer, BUFFER_SIZE, data);
     map_size = atoi(buffer);
     
     map_area = dim * dim;
     
     map = malloc((size_t)BUFFER_SIZE);
     if (!map) {
	  printf("Error allocating memory\n");
	  exit(1);
     }
     
     fgets(buffer, BUFFER_SIZE, data);
     // remove new line from buffer
     buffer[strcspn(buffer, "\n")] = '\0';
     strcpy(map, buffer);
     
     fclose(data);
}

void map_init() {
     // TODO: Only store every block that is not NONE so collision check goes through a smaller array
     map_block = malloc(sizeof(Block) * (size_t)map_area);
     for (int i = 0; i < dim; i++) {
	  for (int j = 0; j < dim; j++ ) {
	       Rectangle rect = {(float)(j * map_size), (float)(i * map_size), (float)map_size, (float)map_size};
	       Type type;
	       switch (map[i * dim + j]) {
	       case '0': type = NONE; break;
	       case '1': type = WALL; break;
		    // temporary make anything else a NONE
	       default:  type = NONE; break;
	       }
	       map_block[i * dim + j] = (Block){rect, type};
	  }
     }
}

void game_init() {
     // TODO: Load this data from config file
     player.x = 132.0f;
     player.y = 132.0f;
     player.angle = 90.0f;

     load_data();
     map_init();

     printf("dim: %d\n", dim);
     printf("map_count: %d\n", map_area);
     printf("map: %s\n", map);
     
     int i = 0;
     while (i < dim * dim) {
	  if (i != 0 && i % dim == 0) printf("\n");
	  printf("%c ", map[i]);
	  i++;
     }
     printf("\n");
}

bool player_check_collision(Type type) {
     // TODO: Make a second collision check with circle that will be the direction player is facing
     float padding = 5;
     for (int i = 0; i < map_area; i++) {
	  if (CheckCollisionCircleRec((Vector2){player.x, player.y}, padding, map_block[i].body)) {
	       if (map_block[i].type == type) {
		    return true;
	       }
	  }
     }
     return false;
}

void player_update() {
     if (IsKeyDown(KEY_A)) {
	  player.angle -= TURN_SPEED * GetFrameTime();
	  if (player.angle < 0) {
	       player.angle += 360;
	  }
     }
     
     if (IsKeyDown(KEY_D)) {
	  player.angle += TURN_SPEED * GetFrameTime();
	  if (player.angle >= 360) {
	       player.angle -= 360;
	  }
     }
     
     const float dx = MOVE_SPEED * cosf(player.angle * DEG2RAD) * GetFrameTime();
     const float dy = MOVE_SPEED * sinf(player.angle * DEG2RAD) * GetFrameTime();
     
     if (IsKeyDown(KEY_W)) {
	  player.x += dx;
	  if (player_check_collision(WALL)) {
	       player.x -= dx;
	  }
	  player.y += dy;
	  if (player_check_collision(WALL)) {
	       player.y -= dy;
	  }
     }
     
     if (IsKeyDown(KEY_S)) {
	  player.x -= dx;
	  if (player_check_collision(WALL)) {
	       player.x += dx;
	  }
	  player.y -= dy;
	  if (player_check_collision(WALL)) {
	       player.y += dy;
	  }
     }
}

void ray_update() {
     float rx, ry, ra, ox, oy, dist;
     int depth;
     pixel_count = 0;
     
     float ray_scale = (float)GetScreenWidth() / RAY_COUNT;
     float inc = 60.0f/RAY_COUNT;
     
     ra = player.angle - 30;
     if (ra > 360) ra -= 360;
     if (ra < 0) ra += 360;
     
     for (int i = 0; i <= RAY_COUNT; i++) {
	  float hx, hy;
	  float dist_h = 10000;
	  float _atan = -1/tanf(ra * DEG2RAD);
	  depth = 0;
	  // facing up
	  if (ra > 180) {
	       ry = (float)(((int)player.y>>(int)log2(map_size))<<(int)log2(map_size));
	       rx = (player.y-ry)*_atan+player.x;
	       
	       oy = -(float)map_size;
	       ox = -oy*_atan;
	  }
	  // facing down
	  if (ra < 180) {
	       ry = (float)(((int)player.y>>(int)log2(map_size))<<(int)log2(map_size)) + (float)map_size;
	       rx = (player.y-ry)*_atan+player.x;
	       
	       oy = (float)map_size;
	       ox = -oy*_atan;
	  }
	  // facing left or right
	  if (ra == 0 || ra == 360 || ra == 180) {
	       rx = player.x;
	       ry = player.y;
	       depth = 45;
	  }
	  
	  // calculating horizontal distance
	  while (depth < 45) {
	       int x = (int)rx/map_size;
	       int y = (int)ry/map_size;
	       if (x < 0 || x > dim || map[y * dim + x] == '1' || map[(y-1) * dim + x] == '1') {
		    hx = rx;
		    hy = ry;
		    dist_h = Vector2Distance((Vector2){player.x, player.y}, (Vector2){hx, hy});
		    depth = 45;
	       }
	       else {
		    rx += ox;
		    ry += oy;
		    depth++;
	       }
	  }
	  // DrawLineEx((Vector2){player.position.x-1, player.position.y-1}, (Vector2){rx-1, ry-1}, 3, GREEN);
	  
	  float vx, vy;
	  float dist_v = 10000;
	  float _ntan = -tanf(ra * DEG2RAD);
	  depth = 0;
	  
	  // looking left
	  if (ra > 90 && ra < 270) {
	       rx = (float)(((int)player.x>>(int)log2(map_size))<<(int)log2(map_size));
	       ry = (player.x-rx) * _ntan + player.y;
	       
	       ox = -(float)map_size;
	       oy = -ox * _ntan;
	  }
	  // looking right
	  if (ra < 90 || ra > 270) {
	       rx = (float)(((int)player.x>>(int)log2(map_size))<<(int)log2(map_size)) + (float)map_size;
	       ry = (player.x-rx) * _ntan + player.y;
	       
	       ox = (float)map_size;
	       oy = -ox * _ntan;
	  }
	  // looking straight up or down
	  if (ra == 270 || ra == 90) {
	       rx = player.x;
	       ry = player.y;
	       depth = 45;
	  }
	  
	  while (depth < 45) {
	       int x = (int)rx/map_size;
	       int y = (int)ry/map_size;
	       if (y < 0 || y > dim || map[y * dim + x] == '1' || map[y * dim + (x-1)] == '1') {
		    vx = rx;
		    vy = ry;
		    dist_v = Vector2Distance((Vector2){player.x, player.y}, (Vector2){vx, vy});
		    depth = 45;
	       }
	       else {
		    rx += ox;
		    ry += oy;
		    depth++;
	       }
	  }
	  // DrawLineEx((Vector2){player.position.x+1, player.position.y+1}, (Vector2){rx+1, ry+1}, 3, RED);
	  
	  // determine which ray to use based on distance
	  if (dist_h < dist_v) {
	       rx = hx;
	       ry = hy;
	       dist =  dist_h;
	  }
	  else {
	       rx = vx;
	       ry = vy;
	       dist = dist_v;
	  }
	  
	  // DrawLineV(player.position, (Vector2){rx, ry}, ORANGE);
	  
	  // normalize to scale of 0.0 to 1.0
	  float norm = (dist - 1) / (((float)dim - 3) * (float)map_size);

	  // sigmoid function, norm - (higher = slower)
	  float alpha = 1 / (1 + expf(4.0f * (norm - 0.5f)));
	  Color color = Fade(WHITE, alpha);
	  
	  // fix fish eye effect
	  float fish = player.angle-ra;
	  if (fish <= 0) fish += 360;
	  if (fish >= 360) fish -= 360;
	  dist = dist * cosf(fish * DEG2RAD);
	  
	  // generate lines
	  float line_height = (float)(map_size * GetScreenWidth()) / dist;
	  if (line_height > (float)GetScreenHeight()) line_height = (float)GetScreenHeight();
	  
	  float line_offset = ((float)GetScreenHeight() - line_height) / 2.0f;
	  
	  // Draw one pixel (rect) per line height	
	  for (int y = 0; y < line_height; y++) {
		DrawRectangleRec((Rectangle){(float)i * ray_scale, line_offset + (float)y, ray_scale, 1}, WHITE);
		// counting total num of pixels
		pixel_count++;
	  }
	  
	  // DrawLineEx((Vector2){(float)i*ray_scale, line_offset}, (Vector2){(float)i*ray_scale, line_height+line_offset}, ray_scale, color);
	  
	  ra += inc;
	  if (ra >= 360) ra -= 360;
	  if (ra <= 0) ra += 360;
     }
}

void map_update() {
     float ox = (float)(GetScreenWidth() - (dim * map_size)) / 2.0f;
     float oy = (float)(GetScreenHeight() - (dim * map_size)) / 2.0f;
     
     for (int i = 0; i < map_area; i++) {
	  Color color;
	  switch (map_block[i].type) {
	  case NONE: color = BLACK; break;
	  case WALL: color = WHITE; break;
	  }
	  
	  DrawRectangleRec((Rectangle){
		    map_block[i].body.x + ox,
		    map_block[i].body.y + oy,
		    map_block[i].body.width,
		    map_block[i].body.height}, 
	       color);
     }
     
     // draw grid
     int x = (int)ox;
     int y = (int)oy;
     for (int i = 0; i <= dim; i++) {
	  DrawLine((int)ox, y, dim * map_size + (int)ox, y, LIGHTGRAY);
	  y += map_size;
	  DrawLine(x, (int)oy, x, dim * map_size + (int)oy, LIGHTGRAY);
	  x += map_size;
     }
     
     // draw player
     DrawCircle((int)(player.x + ox), (int)(player.y + oy), 5, YELLOW);
     Vector2 direction = {20 * cosf(player.angle * DEG2RAD) + player.x + ox, 20 * sinf(player.angle * DEG2RAD) + player.y + oy};
     DrawLineV((Vector2){player.x + ox, player.y + oy}, direction, RED);
}

void debug_info() {
     char pa[20];
     char py[20];
     char px[20];
     char pixel[10];
     
     int lenx = sprintf(px, "%f", player.x);
     if (lenx == -1) printf("Error converting float %f\n", player.x);
     
     int leny = sprintf(py, "%f", player.y);
     if (leny == -1) printf("Error converting float %f\n", player.y);
     
     int lena = sprintf(pa, "%f", player.angle);
     if (lena == -1) printf("Error converting float %f\n", player.angle);
     
     int lenc = sprintf(pixel, "%d", pixel_count);
     if (lenc == -1) printf("Error converting int %d\n", pixel_count);
     
     DrawFPS(0, 0);
     
     DrawText("X: ", 0, 21, 21, GREEN);
     DrawText(px, 21, 21, 21, GREEN);
     
     DrawText("Y: ", 0, 42, 21, RED);
     DrawText(py, 21, 42, 21, RED);
     
     DrawText("A: ", 0, 63, 21, BLUE);
     DrawText(pa, 21, 63, 21, BLUE);
     
     DrawText("P: ", 0, 84, 21, PINK);
     DrawText(pixel, 21, 84, 21, PINK);
}

void game_update() {
     BeginDrawing();
     {
	  ClearBackground(BLACK);
	  player_update();
	  ray_update();
	  
	  if (IsKeyPressed(KEY_TAB)) {
	       if (minimap) {
		    minimap = false;
	       } else {
		    minimap = true;
	       }
	  }
	  
	  if (minimap) {
	       map_update();
	  }
	  
	  debug_info();
     }
     EndDrawing();
}

void game_free() {
     if (map_block) {
	  free(map_block);
	  printf("map block free\n");
     }
}

int main() {
     SetConfigFlags(FLAG_WINDOW_RESIZABLE);
     InitWindow(SCREEN_WIDTH, SCREEN_HEIGHT, "Main");
     SetTargetFPS(60);
     
     game_init();
     while (!WindowShouldClose()) {
	  game_update();
     }	
     CloseWindow();
     game_free();
     return 0;
}
