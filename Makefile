CFLAGS=-Wall -Wextra -Wconversion -std=c99 -pedantic -ggdb
LIBS=-lraylib -lm

all:
	$(CC) $(CFLAGS) main.c   -o raycaster $(LIBS)
edit:
	$(CC) $(CFLAGS) editor.c -o editor $(LIBS)
clean:
	$(RM) raycaster
	$(RM) editor
